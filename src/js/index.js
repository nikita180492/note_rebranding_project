// массив иконок svg

let x = [".menu-icon"];
x.forEach(item => {
  $(item).each(function() {
    let $img = $(this);
    let imgClass = $img.attr("class");
    let imgURL = $img.attr("src");
    $.get(imgURL, function(data) {
      let $svg = $(data).find("svg");
      if (typeof imgClass !== "undefined") {
        $svg = $svg.attr("class", imgClass + " replaced-svg");
      }
      $svg = $svg.removeAttr("xmlns:a");
      if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
        $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
      }
      $img.replaceWith($svg);
    }, "");
  });
});


// $(".order-registration-list").click(function() {
//   $(".order-type-area").toggleClass("_close");
// });
$(".navigation-dots-menu").click(function(e) {
  e.stopPropagation();
  $(".dot-menu").toggleClass("_open");
});

// $(".input-select").click(function(e) {
//   e.preventDefault();
//   e.stopPropagation();
//   // $(this).addClass("_open");
//   // $(this).find(".options").slideToggle();
// });


$(".input-data").click(function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(".input-data").toggleClass("_open");
  $(".data-result-calendar").slideToggle();
});

$(".nav-button").click(function(e) {
  $(".personal-cabinet-navigation").toggleClass("_open");
});

$(document).ready(() => {
  $(window).resize();
});

const teleportElement = (elPc, elMob, child, size) => {

  let x = elPc.find(child);
  let y = elMob.find(child);
  if ($(window).width() <= size) {
    elMob.append(x);
  }
  if ($(window).width() > size) {
    elPc.append(y);
  }
};

$(window).resize(function() {
  teleportElement($(".teleport-img-pc"), $(".teleport-img-mob"), $(".free-img"), 992);
  teleportElement($(".place-form-desk"), $(".place-form-mob"), $(".free-note-col"), 992);
  teleportElement($(".place-form-desk"), $(".place-form-mob"), $(".christmas-form"), 992);
  teleportElement($(".place-form-modal-desk"), $(".place-form-modal-mob"), $(".free-note-col"), 992);
  teleportElement($(".place-your-order-info-desk"), $(".place-your-order-info-mob"), $(".your-order-info"), 768);
  teleportElement($(".place-helper-desk"), $(".place-helper-mob"), $(".helper"), 992);
  teleportElement($(".place-select-all-filter-desk"), $(".place-select-all-filter-mob"), $(".select-all-filter"), 768);
  teleportElement($(".pc-area-note-pay"), $(".mob-area-note-pay"), $(".note-pay "), 768);
  teleportElement($(".pc-area-network-count"), $(".mob-area-network-count"), $(".page-main-social-network__count "), 992);
  // teleportElement($(".pc-area-btn-rec"), $(".mob-area-btn-rec"), $(".rec-btn "), 576);
});

$("a.open-modal").click(function(event) {
  $(this).modal({
    fadeDuration: 250
  });
  return false;
});

$(".profile-enter-cabinet").click(function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(".drop-window-enter").css({ display: "block" });
});

$(window).click(function() {
  $(".drop-window-enter").css({ display: "none" });
  $(".drop-window-navigation").css({ display: "none" });
  $(".dot-menu").removeClass("_open");
  $(".input-select").removeClass("_open");
  $(".input-select .options").hide();
  // $(".input-select").removeClass("_open").find(".options").css({ display: "none" });
});

$("[name=\"catalog-pc\"]").click((e) => {
  // e.preventDefault();
  e.stopPropagation();
  $(".drop-window-navigation").css({ display: "block" });
});

$("[data-fancybox]").fancybox({
  clickContent: "close",
  buttons: ["close"]
});

let open = false;
$(".js-toggle-calendar-months").click(function() {
  if (!open) {
    $(".js-toggle-calendar-months span").text("Свернуть");
  } else {
    $(".js-toggle-calendar-months span").text("Развернуть");
  }
  open = !open;
  $(".calendar-months").toggleClass("calendar-months--open");
});



