const openLeftMenu=()=>{
  $('.left-mobile-menu-area').addClass("_open")
}
// закрыть меню
const closeLeftMenu=()=>{
  $('.left-mobile-menu-area').removeClass("_open")
  $('.mobile-bottom-search-area').removeClass("_open")
  $('.menu-content').css({ display: "none" });
  $('.mobile-bottom-content').css({ display: "none" });
}

$('.overlay').click(closeLeftMenu)
$('.menu-content-close').click(closeLeftMenu)

// Главная — Меню-каталог-мобильная
$('[name="left-catalog"]').click(()=>{
  openLeftMenu()
  $('.menu-content.catalog-type').css({ display: "block" });
})

// Главная — Меню-каталог-мобильная
$('[name="left-menu-mobile"]').click(()=>{
  openLeftMenu()
  $('.menu-content.menu-type').css({ display: "block" });
})

// Иконы — Фильтр-мобильная
$('[name="left-icon-filter"]').click(()=>{
  openLeftMenu()
  $('.menu-content.select-filter-type').css({ display: "block" });
})

// Молебны — Фильтр-мобильная
$('[name="left-note-filter"]').click(()=>{
  openLeftMenu()
  $('.menu-content.select-filter-note-type').css({ display: "block" });
})

// Именины — Фильтр-мобильная
$('[name="left-name-filter"]').click(()=>{
  openLeftMenu()
  $('.menu-content.select-filter-alf-type').css({ display: "block" });
})

// Поставьте свечку
$('[name="left-put-candle"]').click(()=>{
  openLeftMenu()
  $('.menu-content.select-put-candle').css({ display: "block" });
})

//Регистрация
$('[name="left-sign-in"]').click(()=>{
  openLeftMenu()
  $('.menu-content.menu-content-sign-in').css({ display: "block" });
})

//Вход
$('[name="left-login"]').click(()=>{
  openLeftMenu()
  $('.menu-content.menu-content-login').css({ display: "block" });
})

//Поиск для мобилы
// $('[name="mobile-bottom-search"]').click(()=>{
//   $('.mobile-bottom-search-area').addClass("_open")
//   $('.mobile-bottom-content').css({ display: "block" });
// })
