$(document).ready(() => {

  $(".main-banner").slick(
    {
      dots: false,
      arrows: false,
      adaptiveHeight: false, //подгоняет слайды под размер картинки
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1500,
      easing: "ease",
      infinite: false,
      autoplay: false,
      autoplaySpeed: 5000,
      pauseOnFocus: true,
      pauseOnHover: true,
      pauseOnDotsHover: true,
      draggable: true, //на Пк запрешает мышкой перетягивать слайды
      swing: true, //на Моб запрешает перетягивать слайды
      touchThreshold: 5,
      touchMove: true,
      waitForAnimate: true,
      centerMode: false,
      variableWidth: false,
      rows: 1,
      slidesPreRow: 1,
    }
  );

  $(".recommend-slider").slick(
    {
      dots: false,
      arrows: false,
      adaptiveHeight: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      speed: 1000,
      easing: "ease",
      infinite: true,
      autoplay: false,
      autoplaySpeed: 1000,
      centerMode: false,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true
          }
        }
      ]
    }
  );

  $(".monastery-photo-slick").slick(
    {
      dots: false,
      arrows: false,
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      rows: 1,
      slidesPreRow: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true
          }
        },
      ]
    }
  );

  function resizeSlider() {

    if ($(window).width() <= 992) {
      $(".note-tiles-slider").not(".slick-initialized").slick(
        {
          dots: false,
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          variableWidth: true,
          infinite: true
        });
      $(".slider-holiday-landing").not(".slick-initialized").slick(
        {
          variableWidth: true,
          dots: false,
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          autoplaySpeed: 1000
        }
      );
    } else {
      $(".note-tiles-slider.slick-initialized").slick("unslick");
      $(".slider-holiday-landing.slick-initialized").slick("unslick");
    }
  }

  resizeSlider();

  $(window).resize(function () {
    console.log("resize");
    resizeSlider();
  });

  $(".card-holiday-slider").slick(
    {
      dots: false,
      arrows: false,
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 2,
      rows: 1,
      slidesPreRow: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true
          }
        }
      ]
    }
  );

  $(".filter-slider").slick(
    {
      dots: true,
      arrows: false,
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      slidesPreRow: 1,
      // variableWidth: true,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
  );
  $(".slider-name-certificate").slick(
    {
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      rows: 1,
      slidesPreRow: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }
      ]
    }
  );
  $(".about-fund-slider").slick(
    {
      dots: false,
      arrows: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // variableWidth: true
    }
  );


  $('.calendar-slide').slick({
    centerMode: false,
    slidesToShow: 4,
    dots: false,
    arrows: false,
    swipe: true,
    variableWidth: true,
    infinite: false,
    swipeToSlide: true,
    //adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 420,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });

});


